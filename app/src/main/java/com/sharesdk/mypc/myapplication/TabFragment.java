package com.sharesdk.mypc.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MyPC on 2016/4/11.
 */
public class TabFragment extends Fragment {
    public static final String TITLE = "title";
    private String mTitle = "Defaut Value";
    private List<String> mDatas = new ArrayList<String>();
    private ListView listView;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            mTitle = getArguments().getString(TITLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(),R.layout.tablefragment,null);
        listView = (ListView) view.findViewById(R.id.fragment_listview);
        initData();
        return view;
    }

    private void  initData(){
        for (int i = 0;i<50;i++){
            mDatas.add(mTitle+" -> " + i);
        }
        setAdpter();
    }
    private void setAdpter(){
        listView.setAdapter(new ArrayAdapter<String>(getActivity(),
                R.layout.item, R.id.tv, mDatas) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                //Log.e("tag", "convertView = " + convertView);
                return super.getView(position, convertView, parent);
            }
        });
    }
    public static TabFragment newInstance(String title)
    {
        TabFragment tabFragment = new TabFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TITLE, title);
        tabFragment.setArguments(bundle);
        return tabFragment;
    }
}
