package com.sharesdk.mypc.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import com.sharesdk.mypc.myapplication.view.SimpleViewPagerIndicator;

public class MainActivity extends FragmentActivity {

    private String[] mTitles = new String[] { "简介", "评价", "相关" };
    private SimpleViewPagerIndicator simpleViewPagerIndicator;
    private ViewPager viewPager;
    private TabFragment[] mFragments = new TabFragment[mTitles.length];
    private FragmentPagerAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initFragment();
        initEvents();
    }
    private void initView(){
        simpleViewPagerIndicator = (SimpleViewPagerIndicator) findViewById(R.id.id_stickynavlayout_indicator);
        viewPager = (ViewPager) findViewById(R.id.id_stickynavlayout_viewpager);
    }
    private void initFragment(){
        simpleViewPagerIndicator.setTitles(mTitles);
        for(int i = 0;i<mTitles.length;i++){
            mFragments[i] =  new TabFragment().newInstance(mTitles[i]);
        }
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return mTitles.length;
            }

            @Override
            public Fragment getItem(int position) {
                return mFragments[position];
            }

        };
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(0);
    }

    private void initEvents() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
                simpleViewPagerIndicator.scroll(position, positionOffset);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}
